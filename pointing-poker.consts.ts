export const POKER_ROOMS_GET_ROOMS = 'poker-rooms-get-rooms';
export const POKER_ROOMS_JOIN_ROOM = 'poker-rooms-join-room';
export const POKER_ROOMS_LIST_UPDATED = 'poker-rooms-list-updated';

export const CREATE_POKER_ROOM_CREATE = 'create-poker-room-create';

export const POKER_ROOM_LEAVE_ROOM = 'poker-room-leave-room';
export const POKER_ROOM_ROOM_UPDATED = 'poker-room-room-updated';
export const POKER_ROOM_CAST_VOTE = 'poker-room-cast-vote';
export const POKER_ROOM_REVEAL_VOTE = 'poker-room-reveal-votes';
export const POKER_ROOM_CLEAR_VOTES = 'poker-room-clear-votes';