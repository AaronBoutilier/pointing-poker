var Service = require('node-windows').Service;
 
// Create a new service object
var svc = new Service({
  name:'Pointing Poker Server',
  description: 'The NodeJS server for the internal pointing poker website.',
  script: 'C:\\pointing-poker-server\\dist\\server\\src\\main.js', // this must be pointed at the main.js
});
 
// Listen for the 'install' event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});
 
// install the service
svc.install();