var Service = require('node-windows').Service;
 
// Create a new service object
var svc = new Service({
  name:'Pointing Poker Server',
  description: 'The NodeJS server for the internal pointing poker website.',
  script: 'C:\\pointing-poker-server\\dist\\server\\src\\main.js', // this must be pointed at the main.js
});
 
// Listen for the 'uninstall' event so we know when it is done.
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists); 
});
 
// Uninstall the service.
svc.uninstall();