import { Module } from '@nestjs/common';
import { PokerRoomGateway } from './poker-room.gateway';

@Module({
  components: [PokerRoomGateway],
})
export class PokerRoomModule {}
