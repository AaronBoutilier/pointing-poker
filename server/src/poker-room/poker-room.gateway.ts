import { RoomReveal } from './../../../models/room-reveal.model';
import { UserVote } from './../../../models/user-vote.model';
import {
  WebSocketGateway,
  SubscribeMessage,
  WsResponse,
  WebSocketServer,
  WsException,
} from '@nestjs/websockets';
import { User } from '../../../models/user.model';
import { PokerRoom } from '../../../models/poker-room.model';
import { JoinRoom } from '../../../models/join-room.model';
import { RoomUser } from '../../../models/room-user.model';
import { CreateRoom } from '../../../models/create-room.model';

import {
  CREATE_POKER_ROOM_CREATE,

  POKER_ROOMS_GET_ROOMS,
  POKER_ROOMS_JOIN_ROOM,

  POKER_ROOM_CLEAR_VOTES,
  POKER_ROOM_REVEAL_VOTE,
  POKER_ROOM_ROOM_UPDATED,
  POKER_ROOM_LEAVE_ROOM,
  POKER_ROOM_CAST_VOTE,
} from './../../../pointing-poker.consts';

@WebSocketGateway()
export class PokerRoomGateway {
  @WebSocketServer() server;

  private rooms: PokerRoom[] = [];


  @SubscribeMessage(POKER_ROOMS_GET_ROOMS)
  onGetRooms(sender) {
    sender.emit(POKER_ROOMS_GET_ROOMS, this.rooms)
  }

  @SubscribeMessage(CREATE_POKER_ROOM_CREATE)
  onCreateRoom(sender, room: CreateRoom) {
    let roomIndex = this.rooms.map(room => room.name).findIndex(name => name === room.roomName);
    if (room && room.roomName && roomIndex == -1) {
      sender.join(room.roomName);
      const newRoom = new PokerRoom();
      newRoom.id = this.rooms.length + 1;
      newRoom.name = room.roomName;

      const newUser = new User();

      newUser.name = room.userName;
      newUser.isHost = true;
      newRoom.users = [newUser];
      this.rooms.push(newRoom);
      sender.emit(CREATE_POKER_ROOM_CREATE, newRoom);
      this.broadcastRooms();
    }
    else
      this.emitException('Invalid room name, or room already exists.');
  }

  @SubscribeMessage(POKER_ROOMS_JOIN_ROOM)
  onJoin(sender, user: JoinRoom) {
    let roomIndex = this.rooms.findIndex(item => item.id === user.roomId);
    if (user && roomIndex != -1) {
      const room = this.rooms[roomIndex];
      let userIndex = room.users.map(user => user.name).indexOf(user.name);
      if (user && userIndex == -1) {
        sender.join(room.name);
        const newUser = new User();
        newUser.name = user.name;
        room.users.push(newUser);
        sender.emit(POKER_ROOMS_JOIN_ROOM, room);

        this.broadcastRoom(room);
      }
      else
        this.emitException('A user with that name is already in this room.');
    }
    else
      this.emitException('There is no room with that Id.');
  }

  @SubscribeMessage(POKER_ROOM_LEAVE_ROOM)
  onLeave(sender, user: RoomUser) {
    let roomIndex = this.rooms.findIndex(item => item.id === user.roomId);
    if (user && roomIndex != -1) {
      const room = this.rooms[roomIndex];
      sender.leave(room.name);
      let userIndex = room.users.map(user => user.name).indexOf(user.userName);
      if (user && userIndex != -1) {
        sender.leave(room.name, () => {
          room.users.splice(userIndex, 1);
          if (!room.users.length)
            this.rooms.splice(roomIndex, 1);

          if (room.users.length == 0)
            this.broadcastRooms();
          else
            this.broadcastRoom(room);
        });
      }
      else
        this.emitException('No user with that name is in this room.');
    }
    else
      this.emitException('Invalid room.');
  }

  @SubscribeMessage(POKER_ROOM_REVEAL_VOTE)
  onReveal(sender, roomReveal: RoomReveal) {

    let roomIndex = this.rooms.findIndex(item => item.id === roomReveal.roomId);
    if (roomIndex != -1) {
      const room = this.rooms[roomIndex];
      room.reveal = roomReveal.reveal;
      this.broadcastRoom(room);
    }
    else
      this.emitException('There is no room with that Id.');
  }

  @SubscribeMessage(POKER_ROOM_CLEAR_VOTES)
  onClearVotes(sender, roomId: number) {

    let roomIndex = this.rooms.findIndex(item => item.id === roomId);
    if (roomIndex != -1) {
      const room = this.rooms[roomIndex];
      room.reveal = false;
      room.users.forEach(user => {
        user.vote = undefined;
      });
      this.broadcastRoom(room);
    }
    else
      this.emitException('There is no room with that Id.');
  }

  @SubscribeMessage(POKER_ROOM_CAST_VOTE)
  onVote(sender, userVote: UserVote) {
    let roomIndex = this.rooms.findIndex(item => item.id === userVote.roomId);
    if (roomIndex != -1) {
      const room = this.rooms[roomIndex];
      let userIndex = room.users.map(user => user.name).indexOf(userVote.userName);

      if (userIndex != -1) {
        const user = room.users[userIndex];
        user.vote = userVote.vote;

        if (room.users.map(user => user.vote).indexOf(undefined) == -1)
          room.reveal = true;

        this.broadcastRoom(room);
      }
      else
        this.emitException('no user with that id exists in this room.');
    }
    else
      this.emitException('There is no room with that Id.');
  }

  private broadcastRoom(room: PokerRoom) {
    this.server.to(room.name).emit(POKER_ROOM_ROOM_UPDATED, room);
  }

  private broadcastRooms() {
    this.server.emit(POKER_ROOMS_GET_ROOMS, this.rooms);
  }

  private emitException(message: string) {
    this.server.emit('exception', message);
  }
}
