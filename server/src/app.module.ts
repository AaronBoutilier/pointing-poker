import { Module } from '@nestjs/common';
import { PokerRoomModule } from './poker-room/poker-room.module';

@Module({
  imports: [PokerRoomModule],
  controllers: [],
  components: [],
})
export class ApplicationModule {}
