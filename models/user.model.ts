
export class User {
  public id: number;
  public name: string;
  public vote?: number;
  public isHost: boolean;
}