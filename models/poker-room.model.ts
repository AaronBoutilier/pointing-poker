import { User } from "./user.model";

export class PokerRoom {
  public id: number;
  public name: string;
  public users: User[];
  public reveal: boolean;
}