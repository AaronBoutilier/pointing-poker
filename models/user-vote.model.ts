export class UserVote {
    roomId: number;
    userName: string;
    vote: number;
}