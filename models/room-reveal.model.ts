export class RoomReveal {
    roomId: number;
    reveal: boolean;
}