import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokerRoomsComponent } from './poker-rooms/poker-rooms.component';
import { CreatePokerRoomComponent } from './create-poker-room/create-poker-room.component';
import { PokerRoomComponent } from './poker-room/poker-room.component';
import { SocketService } from './socket.service';

@NgModule({
  declarations: [
    AppComponent,
    PokerRoomsComponent,
    CreatePokerRoomComponent,
    PokerRoomComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [
    SocketService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
