import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { PokerRoom } from '../../../../models/poker-room.model';
import { JoinRoom } from '../../../../models/join-room.model';
import {
  POKER_ROOMS_JOIN_ROOM,
  POKER_ROOMS_GET_ROOMS,
  POKER_ROOMS_LIST_UPDATED
} from '../../../../pointing-poker.consts';
import { SocketService } from '../socket.service';

@Component({
  selector: 'app-poker-rooms',
  templateUrl: './poker-rooms.component.html',
})
export class PokerRoomsComponent implements OnInit, OnDestroy {

  @Output() roomJoined = new EventEmitter<{room: PokerRoom, name: string}>();

  rooms: PokerRoom[] = [];
  name = '';

  constructor(private socketService: SocketService) { }

  ngOnInit() {

    this.socketService.socket.on(POKER_ROOMS_JOIN_ROOM, (room: PokerRoom) => {
      this.roomJoined.emit({room: room, name: this.name});
    });

    this.socketService.socket.on(POKER_ROOMS_GET_ROOMS, (rooms: PokerRoom[]) => {
      this.rooms = rooms;
    });

    this.socketService.socket.on(POKER_ROOMS_LIST_UPDATED, (rooms: PokerRoom[]) => {
      this.rooms = rooms;
    });

    this.socketService.socket.emit(POKER_ROOMS_GET_ROOMS);
  }

  ngOnDestroy() {
    this.socketService.socket.off(POKER_ROOMS_GET_ROOMS);
    this.socketService.socket.off(POKER_ROOMS_JOIN_ROOM);
    this.socketService.socket.off(POKER_ROOMS_LIST_UPDATED);
  }

  joinRoom(roomId: number, name: string) {
    const joinRoom = new JoinRoom();
    joinRoom.name = this.name;
    joinRoom.roomId = roomId;
    this.socketService.socket.emit(POKER_ROOMS_JOIN_ROOM, joinRoom);
  }
}
