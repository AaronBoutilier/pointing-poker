import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { RoomReveal } from './../../../../models/room-reveal.model';
import { User } from './../../../../models/user.model';
import { PokerRoom } from '../../../../models/poker-room.model';
import { UserVote } from '../../../../models/user-vote.model';
import { SocketService } from '../socket.service';
import {
  POKER_ROOM_ROOM_UPDATED,
  POKER_ROOM_REVEAL_VOTE,
  POKER_ROOM_CLEAR_VOTES,
  POKER_ROOM_CAST_VOTE,
} from '../../../../pointing-poker.consts';

@Component({
  selector: 'app-poker-room',
  templateUrl: './poker-room.component.html',
})
export class PokerRoomComponent implements OnInit, OnDestroy {

  @Input() room: PokerRoom;
  @Input() user: User;

  votes: VoteSummary[];

  constructor(private socketService: SocketService) { }

  ngOnInit() {
    this.socketService.socket.on(POKER_ROOM_ROOM_UPDATED, (room: PokerRoom) => {
        this.room = room;
        if (this.room.reveal) {
          const a: VoteSummary[] = [];
          const votes = this.room.users.map(user => user.vote);
          this.room.users.map(user => {
            const voteIndex = a.findIndex(vote => vote.vote === user.vote);
            if (voteIndex !== -1) {
              a[voteIndex].count += 1;
            } else {
              a.push({ vote: user.vote, count: 1 });
            }
          });
          a.sort((x, y) => y.count - x.count);
          this.votes = a;
        }
    });
  }

  ngOnDestroy(): void {
    this.socketService.socket.off(POKER_ROOM_ROOM_UPDATED);
  }

  toggleReveal() {
    const roomReveal = new RoomReveal();
    roomReveal.roomId = this.room.id;
    roomReveal.reveal = !this.room.reveal;
    this.socketService.socket.emit(POKER_ROOM_REVEAL_VOTE, roomReveal);
  }

  clearVotes() {
    this.socketService.socket.emit(POKER_ROOM_CLEAR_VOTES, this.room.id);
  }

  vote(vote?: number) {
    const userVote = new UserVote();
    userVote.roomId = this.room.id;
    userVote.userName = this.user.name;
    userVote.vote = vote;
    this.socketService.socket.emit(POKER_ROOM_CAST_VOTE, userVote);
  }
}

export class VoteSummary {
  vote: number;
  count: number;
}
