import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePokerRoomComponent } from './create-poker-room.component';

describe('CreatePokerRoomComponent', () => {
  let component: CreatePokerRoomComponent;
  let fixture: ComponentFixture<CreatePokerRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePokerRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePokerRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
