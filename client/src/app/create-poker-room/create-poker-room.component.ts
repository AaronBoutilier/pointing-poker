import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { CREATE_POKER_ROOM_CREATE } from './../../../../pointing-poker.consts';
import { PokerRoom } from '../../../../models/poker-room.model';
import { SocketService } from '../socket.service';

@Component({
  selector: 'app-create-poker-room',
  templateUrl: './create-poker-room.component.html',
})
export class CreatePokerRoomComponent implements OnInit, OnDestroy {

  @Output() roomCreated = new EventEmitter<{room: PokerRoom, name: string}>();

  roomName = '';
  userName = '';

  constructor(private socketService: SocketService) { }

  ngOnInit() {
    this.socketService.socket.on(CREATE_POKER_ROOM_CREATE, (room: PokerRoom) => {
      this.roomCreated.emit({room: room, name: this.userName});
    });
  }

  ngOnDestroy() {
    this.socketService.socket.off(CREATE_POKER_ROOM_CREATE);
  }

  createRoom() {
    this.socketService.socket.emit(CREATE_POKER_ROOM_CREATE, { roomName: this.roomName, userName: this.userName });
  }
}
