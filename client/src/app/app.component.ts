import { Component, OnInit } from '@angular/core';
import { PokerRoom } from '../../../models/poker-room.model';
import { RoomUser } from '../../../models/room-user.model';
import { User } from '../../../models/user.model';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { SocketService } from './socket.service';
import { POKER_ROOM_LEAVE_ROOM } from '../../../pointing-poker.consts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

  room: PokerRoom;
  user: User;

  constructor(private socketService: SocketService) { }

  ngOnInit(): void {
    window.onbeforeunload = (e) => {
      this.leaveRoom();
    };
  }

  joinRoom(roomAndName) {
    this.user = roomAndName.room.users.find(f => f.name === roomAndName.name);
    this.room = roomAndName.room;
  }

  leaveRoom() {
    if (this.room) {
      const roomUser = new RoomUser();
      roomUser.userName = this.user.name;
      roomUser.roomId = this.room.id;
      this.socketService.socket.emit(POKER_ROOM_LEAVE_ROOM, roomUser);
      this.user = null;
      this.room = null;
    }
  }
}
