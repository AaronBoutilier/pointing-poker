import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable()
export class SocketService {

  private url = 'http://192.168.2.21:3000';
  public socket;

  constructor() {
    this.socket = io(this.url);
  }
}
